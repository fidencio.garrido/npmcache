const NPMService = require("./service"),
	logger = require("winston"),
	pjson = require("../package.json"),
	service = new NPMService();

const NPMRoutes = {
	initRoutes(app){
		app.put("/-/user/:user", (req, res)=>{
			const payload = req.body,
				user = req.params.user;
			service.addUser(user, payload).then((response)=>{
				logger.info(response);
				res.status(201).send(response);
			}).catch((err)=>{
				logger.error(err);
				res.status(401).send("UNAUTHORIZED");
			});
		});

		app.get("/:package", (req, res)=>{
			const pack = req.params.package;
			service.getPackage(pack).then( (packinfo)=>{
				res.send(packinfo);
			}).catch((err)=>{
				res.status(500).send(err);
			});
		});

		app.put("/:package", (req, res)=>{
			const pack = req.params.package,
				headers = {
					authorization: req.headers.authorization,
					"npm-session": req.headers["npm-session"]
				};
			service.publishPackage(pack, req.body, headers).then((response)=>{
				res.send(response);
				service.remove(pack).then(()=>{
					logger.info(`Purged ${pack} after publishing new version`);
				}).catch((e)=>{
					logger.error(e);
				});
			}).catch((err)=>{
				res.status(err.statusCode).send(err);
			});
		});

		app.get("/api/packages", (req, res)=>{
			let q = req.query.package || null,
				page = req.query.pageSize || 20,
				offset = req.query.offset || 0;
			service.findPackages({packname: q, offset: offset, page: page}).then( (packages)=>{
				res.send(packages);
			}).catch( (err)=>{
				res.status(500).send(err);
			});
		});

		app.post("/api/packages", (req, res)=>{
			const p = req.body;
			service.cachePackage(p).then( (cacheres)=>{
				p.status = cacheres;
				res.send(p);
			}).catch( (err)=>{
				res.status(500).send(err);
			});
		});

		app.delete("/api/packages/:packagename", (req, res)=>{
			const p = req.params.packagename;
			service.remove(p).then(()=>{
				res.status(204).send();
			}).catch(()=>{
				res.status(500).send();
			});
		});
		
		app.get("/api/health", (req, res)=>{
			const health = {
				version: pjson.version,
				name: pjson.name
			};
			res.send(health);
		});
	},
	init(app){
		this.initRoutes(app);
	}
};

module.exports = NPMRoutes;