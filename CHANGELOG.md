# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [0.5.1] - 2018-09-05
### Fixed
- Replaced superagent calls with request to solve private packages info
- Enhanced performance by using keep alive agent

## [0.4.0] - 2017-02-28
### Added
- Adding ```DELETE /api/package/{packagename}``` to remove the given package from
cache
- Adding ```GET /api/health``` to return version info

## [0.3.0] - 2017-02-25
### Added
- Users management and package publishing is passed to the main server

### Removed
- Saving users to DB

## [0.2.0] - 2017-02-15
### Added
- Saving users to DB

## [0.1.0] - 2017-02-14
### Added
- Caching NPM Responses