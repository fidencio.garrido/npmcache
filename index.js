const express = require("express"),
	morgan = require("morgan"),
	env = process.env.NODE_ENV || "development",
	pug = require("pug"),
	logger = require("winston"),
	bodyparser = require("body-parser"),
	compression = require("compression"),
	PORT = process.env.PORT || 8080;
	
const app = express(),
	npm = require("./npm/routes"),
	views = require("./views/routes");

app.disable('etag').disable('x-powered-by');
app.use( morgan("short") );
app.use( compression() );
app.use( bodyparser.json() );
app.set( "view engine", "pug");

npm.init(app);
views.init(app);

app.listen(PORT, ()=>{
	logger.info(`PORT=${PORT}`);
});