const basepath = "/cache",
	path = require("path"),
	logger = require("winston"),
	root = path.dirname(__dirname),
	download = require("download"),
	fs = require("fs");

class NPMClient {
	
	createDir(dir){
		logger.info(`CREATE_DIR=${dir}`);
		let r = fs.mkdirSync(dir);
		if (typeof r !== undefined){
			return true;
		} else{
			return false;
		}
	}
	
	exists(fn, isFile, create = false, buildPath = false){
		const filename = (buildPath) ? `${root}${basepath}/${this.name}/${this.version}/${fn}` : fn;
		try{
			fs.accessSync(filename, fs.constants.F_OK);
			return true;
		} catch(e){
			if (!isFile && create===true){
				return this.createDir(filename);
			} else{
				return false;
			}
		}
	}
	
	download(file, target){
		logger.info(file);
		return new Promise( (resolve, reject)=>{
			download(file, target).then(() => {
			   resolve();
			}).catch(()=>{
				reject();
			});
		});
	}
	
	getTarballFn(location){
		const tarparts = location.split("/"),
			tarname = tarparts[tarparts.length-1];
		return tarname;
	}
	
	isCached(autodownload){
		const name = this.name,
			version = this.version,
			tarname = this.getTarballFn(this.location);
			
		const validator = {
			name: this.exists(`${root}${basepath}/${name}`, false, autodownload),
			version: this.exists(`${root}${basepath}/${name}/${version}`, false, autodownload),
			tarball: this.exists(`${root}${basepath}/${name}/${version}/${tarname}`,true, false)
		};
		
		
		return new Promise( (resolve, reject)=>{
			if (validator.name && validator.version && !validator.tarball){
				if (autodownload === true){
					this.download(this.location, `${root}${basepath}/${name}/${version}`).then(()=>{
						resolve();
					}).catch(()=>{
						reject();
					});
				} else{
					reject();
				}
			} else{
				if (validator.name && validator.version && validator.tarball){
					resolve();
				} else{
					reject();
				}
			}
		});
	}
	
	tarballIt(data){
		const name = this.name,
			version = this.version,
			self = this;
		return new Promise( (resolve, reject)=>{
			const d = new Buffer(data, "base64"),
				fn = `${name}-${version}.tgz`;

			self.exists(`${root}${basepath}/${self.name}`, false, true, false);
			self.exists(`${root}${basepath}/${self.name}/${version}`, false, true, false);
			fs.writeFile(`${root}${basepath}/${self.name}/${self.version}/${fn}`, d, {}, (err)=>{
				if (err){
					logger.error(`${root}${basepath}/${self.name}/${self.version}/${fn} cannot be created:`);
					logger.error(err);
					reject(err);
				} else{
					resolve();
				}
			});
		});
	}
	
	constructor(name, version, location){
		this.name = name;
		this.version = version;
		this.location = location;
	}
}

module.exports = NPMClient;