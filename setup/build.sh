rm app.tgz
VERSION=0.5.0
docker rmi elfido/npmcache:latest
docker rmi elfido/npmcache:$VERSION
echo Packaging
cd .. && npm pack && mv *.tgz setup/app.tgz
echo Creating Docker Image
cd setup && docker build -t=elfido/npmcache:latest -t=elfido/npmcache:$VERSION .
echo Post Cleanup
rm app.tgz
