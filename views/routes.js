const vpath = "./pug";

const Views = {
	views:[
		{path: "/--/admin", file: "admin"}
	],
	initViews(app){
		Views.views.forEach( (v)=>{
			app.get(v.path, (req, res)=>{
				res.render(`${vpath}/${v.file}`, {});
			});
		});
	},
	initDefaultPages(app){
		app.use((req, res)=>{
			res.status(404).render(`${vpath}/404`, {});
		});
		
		app.use((req, res)=>{
			res.status(500).render(`${vpath}/500`, {});
		});
	},
	init(app){
		this.initViews(app);
		this.initDefaultPages(app);
	}
};

module.exports = Views;