const http = require('http');
const https = require('https');

const Das = require("../db/das"),
	das = new Das(),
	userDas = new Das("user", {expiry: false}),
	logger = require("winston"),
	request = require("superagent"),
	requestlib = require('request'),
	Client = require("./npmclient"),
	PUBLIC_REGISTRY = "http://registry.npmjs.org";

const URL = process.env.NPMREG || PUBLIC_REGISTRY;
const PUBLISH_URL = process.env.NPMPUBLISH || URL;
const keepAliveAgent = new http.Agent({ keepAlive: true, maxSockets: 10 });
const keepAliveAgentS = new https.Agent({ keepAlive: true, maxSockets: 10 });

class NPMService {
	/**
	 * Reduce size as much as possible to improve response time
	 */
	_cleanPackage(pack){
		const cleanupSections = ["users", "maintainers","contributors"];
		pack.readme = "cached";
		cleanupSections.forEach((section) => {
			if (Array.isArray(pack[section]) && pack[section].length>0){
				pack[section] = [pack[section][0]];
			}
		});
		const versions = Object.keys(pack.versions);
		versions.forEach((version) => {
			if (Array.isArray(pack.versions[version].contributors) && pack.versions[version].contributors.length>0){
				pack.versions[version].contributors = [pack.versions[version].contributors[0]];
			}
			if (Array.isArray(pack.versions[version].maintainers) && pack.versions[version].maintainers.length>0){
				pack.versions[version].maintainers = [pack.versions[version].maintainers[0]];
			}
		});
		pack.users = {cached: true};
		return pack;
	}
	
	_create(pack){
		return new Promise( (resolve, reject)=>{
			const id = (pack._id) ? pack._id : pack.name;
			das.create(id, pack).then( (result)=>{
				resolve(result);
			}).catch( (err)=>{
				logger.error("Error saving package info");
				logger.error(err);
				reject(err);
			});
		});
	}
	
	_getPackageInfo(pack){
		const self = this;
		return new Promise( (resolve, reject)=>{
			let registryUrl = URL,
				isPrivate = (pack.indexOf('@')===0);
			if (isPrivate === true){
				registryUrl = PUBLIC_REGISTRY;
				pack = pack.replace('/', '%2f');
				logger.info(`looking for private package: ${pack}`);
			}
			const url = `${registryUrl}/${pack}`;
			const options = {
				url: url,
				agent: (url.indexOf('https') === 0) ? keepAliveAgentS : keepAliveAgent,
				json: true,
				time: true,
			}
			requestlib(options, (err, response, body) => {
				if (err) {
					reject(err);
				}
				else {
					try {
						const json = this._cleanPackage(body);
						const depid = (json._id) ? json._id : json.name;
						self._create(json).then(()=>{
							logger.info(`CACHING_ITEM=${depid}`);
						}).catch((err)=>{
							logger.error("cant save", err);
						});
						logger.info(`response=${response.timings.response}`);
						resolve(json);
					}
					catch(e){
						logger.error(`Error for ${pack} ${e}`);
						reject(e);
					}
				}
			});
		});
	}
	
	findPackages(packfilter){
		logger.info("looking for package");
		let q = (packfilter.packname === null) ? "_meta.type='npm'" : `_meta.type='npm' and item._id like "%${packfilter.packname}%"`;
		return new Promise( (resolve, reject)=>{
			das.findAll(q, {page: packfilter.page, offset: packfilter.offset}).then((items)=>{
				resolve(items);
			}).catch((err)=>{
				reject(err);
			});
		});
	}
	
	getPackage(packname){
		const self = this;
		return new Promise( (resolve, reject)=>{
			if (!packname){
				reject("package name is required");
			} else{
				das.findById(packname).then( (info)=>{
					const id = info.item._id || info.item.name;
					logger.info(`CACHE_READ=${id}`);
					resolve(info.item);
				}).catch((e)=>{
					logger.info(`Looking on NPM for ${packname} ${e}`);
					self._getPackageInfo(packname).then( (info)=>{
						resolve(info);
					}).catch((err)=>{
						logger.error(`Cannot find package ${packname} ${err}`);
						reject("Cannot find package");
					});
				});
			}
		});
	}

	cachePackage(packinfo){
		const self = this;
		return new Promise( (resolve, reject)=>{
			self.getPackage(packinfo.name).then( (res)=>{
				const versions = Object.keys(res.versions);
				let tarball = "";
				versions.forEach( (version)=>{
					if (version===packinfo.version){
						tarball = res.versions[version].dist.tarball;
					}
				});
				const client = new Client(packinfo.name, packinfo.version, tarball);
				client.isCached(true).then(()=>{
					resolve(tarball);
				}).catch(()=>{
					reject();
				});
			}).catch((err)=>{
				reject(err);
			});
		});
	}
	
	addUser(user,payload){
		return new Promise((resolve, reject)=>{
			request.put(`${PUBLISH_URL}/-/user/${user}`).send(payload).end((err, res)=>{
				if(err){
					logger.error(`Failed to login ${err}`);
					reject(res);
				}
				resolve(res.text);
			});
		});
	}

	publishPackage(name, packageInfo, authHeaders){
		return new Promise((resolve, reject)=>{
			request.put(`${PUBLISH_URL}/${name}`).set(authHeaders).send(packageInfo).end((err, res)=>{
				if(err){
					logger.error(`Failed to publish ${err}`);
					reject(res);
				}
				resolve(res.text);
			});
		});
	}
	
	remove(packname){
		return new Promise((resolve, reject)=>{
			das.remove(packname).then(()=>{
				resolve();
			}).catch((err)=>{
				logger.error(err);
				reject(err);
			});
		});
	}
}

module.exports = NPMService;