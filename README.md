# NPM Cache
Cache your NPM dependencies metadata in the best NoSQL database; boost your
enterprise NodeJS pipeline.

## Execution

Requires NodeJS 6.

```sh
node index
```

Or even better, use the official Docker image (https://hub.docker.com/r/elfido/npmcache/)
```sh
docker run -d -p 8080:8080 -e "DB=mycouchbase.mydomain.com" elfido/npmcache:latest
```

## Features

| Feature      | Status |
| ------------ | ------ |
| Cache package information | Available |
| Docker Image (elfido/npmcache:latest) | Available |
| Publishing proxy | Available |

## Configuration values

The following values are supported through environment variables.

| Variable | Type  | Notes |
| -------- |------ |-------|
| TTL   | Number| Cache time in hours |
| DB    | String| DB hostname (no port needed) |
| BUCKET | String | Couchbase bucket |
| NPMREG | String | NPM Registry (default http://registry.npmjs.org)|
| NPMPUBLISH | String | Server for publishing, defaults to NPMREG |


## Usage

Example of Docker compose file to use it:

```yaml
version: "2"
services:
 npm:
  image: elfido/npmcache:0.4.0
  ports:
   - "8080:8080"
  environment:
   - "DB=couchbase-server"
   - "BUCKET=default"
  links:
   - "couchbase:couchbase-server"
  depends_on:
   - "couchbase"
 couchbase:
  image: couchbase:community-4.5.0
  ports:
   - "8091-8094:8091-8094"
   - "9100-9105:9100-9105"
   - "9998-9999:9998-9999"
   - "11207-11215:11207-11215"
```