const couchbase = require("couchbase"),
	N1ql = couchbase.N1qlQuery,
	logger = require("winston"),
	HOURS_FACTOR = 3600,
	BUCKET_NAME = process.env.BUCKET || "npmcache",
	EXPIRY = (process.env.TTL * HOURS_FACTOR) || 7200, // Default two hours
	DBHOST = process.env.DB || "localhost";

class DAS {
	constructor(doctype = "npm", options = {expiry: true}){
		logger.info(`DB=${DBHOST} BUCKET=${BUCKET_NAME} doctype=${doctype}`);
		this.doctype = doctype;
		this.saveoptions = (options.expiry === true) ? {expiry: EXPIRY} : {};
		this.cluster = new couchbase.Cluster(`couchbase://${DBHOST}`);
		this.bucket = this.cluster.openBucket(BUCKET_NAME, (err)=>{
			if (err){
				logger.error("Cannot connect to bucket");
			} else{
				logger.info("Connected to couchbase");
			}
		});
	}
	
	findAll(query, opts){
		const self = this;
		return new Promise( (resolve, reject)=>{
			let q = `select item.* from ${BUCKET_NAME} where ${query} limit ${opts.page} offset ${opts.offset}`,
				n1ql = N1ql.fromString(q);
			logger.info(q);
			self.bucket.query(n1ql, (err, response)=>{
				if (err){
					reject(err);
				}
				resolve(response);
			});
		});
	}
	
	findById(id){
		const self = this;
		return new Promise((resolve, reject)=>{
			const start = process.hrtime();
			this.bucket.get(id, (err, item)=>{
				const ended = process.hrtime(start);
				logger.info(`Couchbase response time: ${ended[0]}s ${ended[1]/1000000}ms`);
				if (err){
					if(typeof err==="object" && typeof err.code !== "undefined"){
						if (err.code === "13"){
							logger.error(err);
						}
					}
					reject(err);
				}
				if (item!==null && item.value){
					if (item.value._meta.type === self.doctype){
						resolve(item.value);
					} else{
						reject("NOT_AUTHORIZED");
					}
				} else{
					reject("NOT_FOUND");
				}
			});
		});
	}
	
	create(id, doc){
		const item = {
				_meta: {
					created: new Date(),
					type: this.doctype
				},
				item: doc
			},
			self = this;
		
		return new Promise( (resolve, reject)=>{
			//Expiry seems to be expressed in seconds
			self.bucket.insert(id, item, self.saveoptions, (err, result)=>{
				if (err){
					reject(err)
				}
				resolve(result);
			});
		});
	}
	
	remove(id){
		const self = this;
		return new Promise((resolve, reject)=>{
			logger.info(`CLEAR=${id}`);
			self.bucket.remove(id, ()=>{
				resolve();
			});
		});
	}
}

module.exports = DAS;